
$(document).ready(function(){
    $('.slider, .slider-mob').slick({
        rtl: true
    });
  });

  $(document).ready(function(){
    $('#toggle').click(function() {
          $('#toggle').css('display','none')
          $('#text').slideDown();
    })
  });

  //smooth scrol to ankor safari iphone and all other

$('.topnav-ul').on('click', 'a', function() {
  let anchorId = $(this).attr('href');
  scrollingDistance = $(anchorId).offset().top;
  $('html, body').animate({scrollTop: scrollingDistance}, 600);
});

//navigation onscroll

window.onscroll = function() {
  scrollFunction(),onScroll()
 };

 function scrollFunction() {
   if ($(window).width()>999){
   if (document.body.scrollTop > 400 || document.documentElement.scrollTop > 600) {
     $('.topnav-ul').addClass('active-scroll');
   } else {
     $('.topnav-ul').removeClass('active-scroll');
   }
 }
}


 function onScroll(event){
  let scrollPos = $(document).scrollTop();
  $('.topnav-a').each(function () {
      let currLink = $(this);
      let refElement = $(currLink.attr('href'));
      if (refElement.position().top <=scrollPos+100 && refElement.position().top + refElement.height() > scrollPos+100) {
          $('.topnav-a').removeClass('active-a');
          currLink.addClass('active-a');
      } else{
          currLink.removeClass('active-a');
      }
  });
}

 //menu toggle

(function($){
  $('.nav-toggle,.topnav-a').click(function(){
    $('.topnav-ul').toggleClass('active');
    $('.nav-toggle').toggleClass('active');
    $('.menu-overlay').toggleClass('active');

  })
})(jQuery);



//menu click close if out of aria

$(document).click(function(event) {
  //if you click on anything except the modal itself or the "open modal" link, close the modal
  if (!$(event.target).closest('.topnav-ul,.topnav-a,.nav-toggle').length && $('.topnav-ul').hasClass('active')) {
    $('body').find('.topnav-ul,.nav-toggle,.menu-overlay').toggleClass('active');
  }
});